/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import dao.CategoriaImp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import util.Dao;

/**
 *
 * @author fredy.alfarousam
 */
@Configuration
@ComponentScan(basePackages = {"dao","controller"})
public class Config{

    @Bean
    public Dao daoCategoria()  {
        return new CategoriaImp();
    }

    @Bean
    public Connection getConex() {
        Connection conn;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/participante26?useSSL=false", "root", "root");
        } catch (SQLException e) {
            conn = null;
        } catch (ClassNotFoundException cx) {
            conn = null;
        }
        return conn;
    }
}
