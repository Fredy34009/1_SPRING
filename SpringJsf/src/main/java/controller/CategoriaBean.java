/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import util.Dao;

/**
 *
 * @author fredy.alfarousam
 */
@Controller
@RequestScoped
@ManagedBean(name = "categoriaMB")
public class CategoriaBean {
    
    @ManagedProperty("#{daoCategoria}")
    private Dao daoCategoria;

    public Dao getDaoCategoria() {
        return daoCategoria;
    }

    public void setDaoCategoria(Dao daoCategoria) {
        this.daoCategoria = daoCategoria;
    }
    
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    public void hola()
    {
        msg="Hola Spring";
    }
    
}
